(defproject io.gitlab.k3rni/leben "0.1.0"
  :description "An implementation of Conway's Life in Clojure"
  :url "http://gitlab.com/k3rni/leben"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :profiles { :uberjar {:aot :all}}
  :aot [:all]
  :main leben.demo)

(ns leben.core)

; Use a hashmap for a board, therefore dimensions can be ignored
(defn create-board [_ _] (hash-map))

(defn width-of [board]
  (let [rows (vals board)] ; Rows are our nested hashmap's first level values
    (inc ; One more than
      (apply max ; the maximum of given list
             (mapcat keys rows))))) ; concatenated keys = column indexes from all rows

(defn height-of [board]
  (inc ; One more than
    (apply max (keys board)))) ; the maximum of list of row numbers

; Fetch value at given coordinates, returns a boolean
(defn at-board [board x y]
  (get-in board [y x] false)) ; get-in does the recursion through nested hashmaps. if not found, returns last arg

(defn set-live! [board x y]
  (update-in board [y] ; update row at y, using a transform function. An empty hashmap is created if missing.
             (fn [row] (assoc row x true)))) ; insert a (index, true) pair into the row's map

(defn set-dead [board x y]
  (update-in board [y] ; Like set-live!, but just remove a pair. Nothing happens if there wasn't one.
             (fn [row] (dissoc row x))))

; Remove all row hashmaps that were empty
(defn wipe-empty [board]
  (select-keys board ; Retain only some rows - whose keys are:
               (for [[k row] board ; listcomp that will contain only indexes of non-blank rows
                     :when (seq row)] ; (seq iterable) is a clojure idiom for checking if not-empty. limit listcomp to only these
                 k))) ; and return only the row index

; Like set-dead, but also drop blank rows for space-saving
(defn set-dead! [board x y]
  (wipe-empty ; wipe the empty hashes but first
    (set-dead board x y))) ; set this one to dead

; Update a position in the map, but using a boolean for live/dead
(defn update-at [board x y new-value]
  (cond new-value (set-live! board x y) ; if new-value then set-live
        :else (set-dead! board x y)))

; Return a list of all the live cell coordinates)
(defn live-cells [board]
  (for [[rownum row] board ; Iterate over the whole nested hashmap
        [cellnum _] row]
    [cellnum rownum])) ; yielding pairs (in that order because it's x y)

; Return a list of given point's neighbor coordinates. This assumes a 2D square grid
(defn neighbor-coords [x y]
  (for [r (range (dec y) (+ 2 y)) ; for r in range(y - 1, y + 2) and the +2 is because ranges are end-exclusive
        c (range (dec x) (+ 2 x)) ; same for c
        :when (or (not= r y) (not= c x))] ; skipping pairs of (r c) that are equal to (x y)
    [c r]))

(defn neighbor-count [board x y]
  (count 
    (filter true? ; Only the live cells
            (map (fn [[x y]] (at-board board x y)) ; extract cell state at given coords
                 (neighbor-coords x y)))))  ; using the list of neighbors

; NOTE: Will contain duplicates, when neighborhoods of live cells overlap. This is harmless the way we update the board later. Turning into a set would require keeping the whole list in memory
; Return a list of interesting locations on the board. This means each live cell plus its immediate neighborhood.
(defn interesting-coords [board]
    (mapcat ; concatenate all the results of applying
      (fn [[x y]] (cons [x y] (neighbor-coords x y))) ; coords of the cell itself plus neighborhood 
            (live-cells board))) ; over all live cells on the board

; Return a rule function of the form (fn [state live-count]) that returns the next state of a cell given its current one and live neighbor count
(defn make-rule [dead-spawns [survive-low survive-high]]
  (fn [state live-count]
    (cond state (<= survive-low live-count survive-high) ; for live cells, consider if neighbor count is between these values
          (not state) (= dead-spawns live-count)))) ; and for dead cells, check for exactly that number of live neighbors

; Conway's classic rule
(def conway (make-rule 3 [2 3]))

; Return this board's next state
(defn tick [board rule]
  (let [to-update (interesting-coords board) ; these are the cell coords we need to recalculate
        old-states (map (fn [[x y]] (at-board board x y)) to-update) ; and their current state
        neighbor-counts (map (fn [[x y]] (neighbor-count board x y)) to-update) ; and their neighbor count
        new-states (map rule old-states neighbor-counts) ; calculate new states using the rule function
        updates (map vector to-update new-states) ; and pair new states with coordinates
        ]
    (reduce ; apply list of updates to the board one at a time
      (fn [board [[x y] new-state]]
        (update-at board x y new-state)) ; by setting given coords to new state
              board ; starting with board as passed
              updates))) ; using the list of updates calculated above

; Clear an xterm and return cursor to topleft position
(defn clear [] (print "\033c"))

; Print a single row, using dots for dead spaces and hashes for live ones
; maxw is the maximum column number to display.
; NOTE: doesn't handle negative indexes in the map. For this to work, should iterate not from zero col, but from global leftmost col.
(defn print-row [maxw row]
  (doall ; Greedily evaluate the exprs. Clojure is lazy by default.
    (for [i (range 0 maxw)
          :let [v (get row i)]]
      (print (if v \# \.)))
    ))

; Print a board, optionally passing desired output width and height. If omitted, will be calculated from board itself.
(defn print-board [board & [width height]]
  (let [w (if width width (width-of board))
        h (if height height (height-of board))]
    (dotimes [r h] ; like a (for) but we're not interested in the result, just the sideeffects
      (print-row w (get board r))
      (println)
      )))

; Return a pre-made board containg a down-right pointed glider
(defn glider-board []
  (-> (create-board 3 3) ; thread: insert this initial board as the first arg of all following forms
                         ; or more precisely: as second item, making it the first arg
      (set-live! 1 0)
      (set-live! 2 1)
      (set-live! 0 2) (set-live! 1 2) (set-live! 2 2)))

; Continuously print a board, while updating it with given rule function. Optionally pass waittime in milis between successive updates.
(defn live! [initial-board rule & [wait]]
  (loop [board initial-board] ; set up a recursion target, and initial values.
    (clear)
    (print-board board)
    (Thread/sleep (if wait wait 500))
    (recur ; jump back to recursion target (`loop`) but could also be just the function top
      (tick board rule)))) ; and reassign board to its updated value

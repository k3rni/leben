# leben

This is a self-education project that implements Conway's Game of Life in Clojure.

## Usage

`lein run` in the console to see a single glider infinitely progressing down and right. But actually,
you should browse the code instead.

## License

Copyright © 2018 Krzysztof Zych

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
